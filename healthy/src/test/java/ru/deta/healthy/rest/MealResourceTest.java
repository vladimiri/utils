package ru.deta.healthy.rest;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ru.deta.healthy.jpa.Meal;
import ru.deta.healthy.jpa.User;
import ru.deta.healthy.rest.param.MealParam;

public class MealResourceTest {

	private MealResource cut;
	private User user;
	MealParam mp = new MealParam();
	EntityManager em = null;

	@Before
	public void initializeDependencies() {
		this.cut = mock(MealResource.class);
		this.user = mock(User.class);
		when(user.getLogin()).thenReturn("login");
		when(cut.getAuthenticatedUser()).thenReturn(user);
		em = mockEntityManager();
		when(cut.getEntityManager()).thenReturn(em);
		mp.setCalories(100);
		mp.setMealDate(new Date());
		mp.setNote("ha-ha-ha");
	}


	@Test
	public void setupUser() {
		User u = new User();
		when(this.cut.getAuthenticatedUser()).thenReturn(u);
		User u2 = this.cut.getAuthenticatedUser();
		Assert.assertEquals(u, u2);
	}

	@Test
	public void createMeal() {
		Mockito.doCallRealMethod().when(cut).createMeal(mp);
		Response resp = cut.createMeal(mp);
		Meal m = (Meal) resp.getEntity();
		Assert.assertNotNull(m);
		verify(em).persist(m);
		Assert.assertEquals(m.getNote(), mp.getNote());
		Assert.assertEquals(m.getCalories(), mp.getCalories());
		Assert.assertEquals(m.getMealDate(), mp.getMealDate());
	}

	@Test
	public void updateMeal() {
		Meal meal = new Meal();
		meal.setCalories(100);
		meal.setId(10);
		meal.setMealDate(new Date());
		meal.setNote("haaaa!");
		meal.setUser(user);
		Mockito.doCallRealMethod().when(cut).updateMeal(10, mp);
		when(em.find(Meal.class, 10)).thenReturn(meal);
		Response resp = cut.updateMeal(10, mp);
		Meal meal1 = (Meal) resp.getEntity();
		verify(em).persist(meal1);
		Assert.assertEquals(meal1.getCalories(), mp.getCalories());
		Assert.assertEquals(meal1.getMealDate(), mp.getMealDate());
		Assert.assertEquals(meal1.getNote(), mp.getNote());

	}

	private EntityManager mockEntityManager() {
		EntityManager em = mock(EntityManager.class);
		return em;
	}


	@Test(expected=NullPointerException.class)
	public void deleteMealThrowsNPE() throws NullPointerException {
		Mockito.doCallRealMethod().when(cut).deleteMeal(10);
		when(em.find(Meal.class,10)).thenReturn(null);
		doThrow(new NullPointerException()).when(cut).deleteMeal(10);
		cut.deleteMeal(10);
	}
	/*
	Iterator mockUser(User consultant) {
		Iterator iterator = mock(Iterator.class);
		when(iterator.next()).thenReturn(consultant);
		when(iterator.hasNext()).thenReturn(true);
		return iterator;

	}

	@Test(expected = IllegalStateException.class)
	public void unreasonablePrediction() {
		Consultant consultant = new Blogger();
		Iterator iterator = mockIterator(consultant);
		when(this.cut.company.iterator()).thenReturn(iterator);
		this.cut.predictFutureOfJava();
	}

	@Test
	public void unreasonablePredictionFiresEvent() {
		Consultant consultant = new Blogger();
		Result expectedResultToFire = consultant.predictFutureOfJava();
		Iterator iterator = mockIterator(consultant);
		when(this.cut.company.iterator()).thenReturn(iterator);
		try {
			this.cut.predictFutureOfJava();
		} catch (IllegalStateException e) {
		}
		verify(this.cut.eventListener, times(1)).fire(expectedResultToFire);

	}*/
}