package ru.deta.healthy.rest;

import static org.mockito.Mockito.*;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.verification.VerificationMode;

import ru.deta.healthy.jpa.Settings;
import ru.deta.healthy.jpa.User;


public class UserResourceTest {
	
	private UserResource cut;
	private User user;
	EntityManager em = null;
	
	
	@Before
	public void initializeDependencies() {
		this.cut = mock(UserResource.class);
		this.user = mock(User.class);
		when(user.getLogin()).thenReturn("login");
		when(cut.getAuthenticatedUser()).thenReturn(user);
		when(user.getSettings()).thenReturn(new Settings());
		em = mockEntityManager();
		when(cut.getEntityManager()).thenReturn(em);
	}

	private EntityManager mockEntityManager() {
		EntityManager em = mock(EntityManager.class);
		return em;
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void updateSettings_withException() {
		Settings set = new Settings();
		set.setDailyCalsLimit(-10);
		when(cut.updateSettings(set)).thenCallRealMethod();
		cut.updateSettings(set);
	}

	public void updateSettings() {
		Settings set = new Settings();
		set.setDailyCalsLimit(10);
		when(cut.updateSettings(set)).thenCallRealMethod();
		try {
			cut.updateSettings(set);
		} catch(IllegalArgumentException e) {
			verify(em,times(1)).persist(user);
		}
	}

}
