package ru.deta.healthy.rest;

import java.security.Principal;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import ru.deta.healthy.Utils;
import ru.deta.healthy.jpa.User;
import ru.deta.healthy.jpa.util.JPAFilter;

@Path("/auth")
public class AuthResource extends BaseResource  {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticate(@QueryParam("login") String login,@QueryParam("password") String password) {
		EntityManager em = JPAFilter.getEntityManager();
		
		if(login == null)
			return Response.status(Status.FORBIDDEN).entity(Messages.getString("AuthResource.0")).build(); //$NON-NLS-1$
		if(password == null)
			return Response.status(Status.FORBIDDEN).entity(Messages.getString("AuthResource.1")).build(); //$NON-NLS-1$
			
		final User user = em.find(User.class, login);
		
		if(checkPassword(user,password)) {
			SecurityContext sc = new SecurityContext() {
	            public Principal getUserPrincipal() {
	                return new UserInfo(user);
	            }

	            public boolean isUserInRole(String role) {
	                return role.equals(role);
	            }

	            public boolean isSecure() {
	                return false;
	            }

	            public String getAuthenticationScheme() {
	                return "custom";
	            }
	        };
			httpRequest.getSession().setAttribute("securityContext", sc);
		} else {
			return Response.status(Status.FORBIDDEN).entity(Messages.getString("AuthResource.4")).build(); //$NON-NLS-1$
		}
		return Response.ok(user).build();
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(User user) {
		user.setPassword(Utils.md5(user.getPassword()));
		EntityManager em = JPAFilter.getEntityManager();
		em.persist(user);
		return Response.ok(user).build();
	}
	
	@GET
	@Path("/check")
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticate() {
		if(super.getAuthenticatedUser() != null) {
			return Response.ok(getAuthenticatedUser()).build();
		} else {
			return Response.status(Status.FORBIDDEN).entity(Messages.getString("AuthResource.4")).build();
		}
	}
	
	private boolean checkPassword(User user, String password) {
		if(user != null && user.getPassword().equalsIgnoreCase(Utils.md5(password)))
			return true;
		return false;
	}
	
	
}
