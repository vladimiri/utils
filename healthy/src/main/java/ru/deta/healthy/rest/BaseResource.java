package ru.deta.healthy.rest;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import ru.deta.healthy.jpa.User;
import ru.deta.healthy.jpa.util.JPAFilter;

public class BaseResource {
	@Context
	protected UriInfo uriInfo;
	@Context
	protected Request request;
	@Context 
	protected HttpServletRequest httpRequest;
	@Context
	ContainerRequestContext requestContext;
	@Context
	SecurityContext securityContext;
	
	public UriInfo getUriInfo() {
		return uriInfo;
	}
	public void setUriInfo(UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public HttpServletRequest getHttpRequest() {
		return httpRequest;
	}
	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.httpRequest = httpRequest;
	}
	public ContainerRequestContext getRequestContext() {
		return requestContext;
	}
	public void setRequestContext(ContainerRequestContext requestContext) {
		this.requestContext = requestContext;
	}
	public SecurityContext getSecurityContext() {
		return securityContext;
	}
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}
	
	public User getAuthenticatedUser() {
		EntityManager em = getEntityManager();
		return em.find(User.class, securityContext.getUserPrincipal().getName());
	}
	
	public EntityManager getEntityManager() {
		return JPAFilter.getEntityManager();
	}
}
