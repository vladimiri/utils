package ru.deta.healthy.rest;

import java.security.Principal;

import ru.deta.healthy.jpa.User;

public class UserInfo implements Principal {
	private User user;

	public UserInfo(User user) {
		this.user = user;
	}

	public String getName() {
		return user.getLogin();
	}

	public String getRole() {
		return "users";
	}

	public int getAccessLevel() {
		return 1;
	}

}