package ru.deta.healthy.rest;

import java.io.IOException;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.AUTHORIZATION)
public class SecureRequestFilter implements ContainerRequestFilter {

	@Context
	HttpServletRequest httpRequest;

	public void filter(ContainerRequestContext requestContext) throws IOException {
		if(!"/auth".equals(httpRequest.getPathInfo())) {
			if(httpRequest.getSession() != null && httpRequest.getSession().getAttribute("securityContext") != null){
				requestContext.setSecurityContext((SecurityContext) httpRequest.getSession().getAttribute("securityContext"));
			}
			SecurityContext securityContext =	requestContext.getSecurityContext();
			if (securityContext == null || !securityContext.isUserInRole("users")) {
				requestContext.abortWith(Response
						.status(Response.Status.UNAUTHORIZED)
						.entity("User cannot access the resource.")
						.build());
			}
		}
		
	}

	
}