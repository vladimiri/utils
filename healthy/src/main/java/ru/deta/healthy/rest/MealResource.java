package ru.deta.healthy.rest;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ru.deta.healthy.jpa.Meal;
import ru.deta.healthy.jpa.User;
import ru.deta.healthy.rest.param.MealParam;

@Path("/meal")
public class MealResource extends BaseResource {
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMeal( MealParam param) {
		User user = getAuthenticatedUser(); 
		EntityManager em = getEntityManager();
		
		Meal meal = new Meal();
		meal.setUser(user);
		meal.setCalories(param.getCalories());
		meal.setNote(param.getNote());
		meal.setMealDate(param.getMealDate());
		em.persist(meal);
		return Response.ok(meal).build();
	}

	@PUT
	@Path("/{mealId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMeal(@PathParam("mealId") int mealId, MealParam param) {
		EntityManager em = getEntityManager();
		Meal meal = em.find(Meal.class, mealId);
		meal.setCalories(param.getCalories());
		meal.setNote(param.getNote());
		meal.setMealDate(param.getMealDate());
		em.persist(meal);
		return Response.ok(meal).build();
	}

	@DELETE
	@Path("/{mealId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteMeal(@PathParam("mealId") int mealId) {
		EntityManager em = getEntityManager();
		Meal meal = em.find(Meal.class, mealId);
		em.remove(meal);
		return Response.ok(meal).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMeals(@PathParam("id") int mealId) {
		EntityManager em = getEntityManager();
		Meal meal = em.find(Meal.class, mealId);
		if(meal != null)
			return Response.ok(meal).build();
		else 
			return Response.status(Status.NOT_FOUND).build();
	}


	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMeals(@QueryParam("from") long from,@QueryParam("to") long to) {
		EntityManager em = getEntityManager();
		User user = getAuthenticatedUser(); 
		List<Meal> list = null;
		if(user == null)
			return Response.status(Status.FORBIDDEN).build();
			
		if(from > 0 && to > 0) {
			Date dateFrom = new Date(from*1000);
			Date dateTo = new Date(to*1000);
			list = em.createQuery("from Meal where user=:user and mealDate between :from and :to")
					.setParameter("user", user)
					.setParameter("from", dateFrom)
					.setParameter("to", dateTo)
					.getResultList();
		} else {
			list = em.createQuery("from Meal where user=:user ")
					.setParameter("user", user)
					.getResultList();
		}
		return Response.ok(list).build();
	}

}





