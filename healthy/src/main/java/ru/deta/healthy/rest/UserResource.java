package ru.deta.healthy.rest;

import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ru.deta.healthy.Utils;
import ru.deta.healthy.jpa.Settings;
import ru.deta.healthy.jpa.User;
import ru.deta.healthy.jpa.util.JPAFilter;

@Path("/user")
public class UserResource extends BaseResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser() {
		User user = getAuthenticatedUser();
		return Response.ok(user).build();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateSettings(Settings set) {
		EntityManager em = getEntityManager();
		if(set.getDailyCalsLimit() <= 0) {
			throw new IllegalArgumentException("calory value should be above zero");
		}
		User user = getAuthenticatedUser();
		user.getSettings().setDailyCalsLimit(set.getDailyCalsLimit());
		em.persist(user);
		return Response.ok(user).build();
	}

	@PUT
	@Path("/changePassword")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePassword(@QueryParam("password") String password) {
		User user = getAuthenticatedUser();
		user.setPassword(Utils.md5(password));
		return Response.ok(user).build();
	}
	
}
