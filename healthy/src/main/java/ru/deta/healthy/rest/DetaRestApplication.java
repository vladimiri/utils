package ru.deta.healthy.rest;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class DetaRestApplication extends ResourceConfig {
	public DetaRestApplication() {
		packages("ru.deta.healthy.rest");
//		register(RolesAllowedDynamicFeature.class); // only to use @RolesAllowed - not required.
		register(DetaMapperProvider.class);
	}
}