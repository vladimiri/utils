package ru.deta.healthy.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="meal")
public class Meal extends BaseId{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4188672369772335281L;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="login")
	@JsonIgnore
	private User user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date mealDate = new Date();
	
	@Column(length=2048)
	private String note;
	private int calories;
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getMealDate() {
		return mealDate;
	}
	public void setMealDate(Date mealDate) {
		this.mealDate = mealDate;
	}

	public int getCalories() {
		return calories;
	}
	public void setCalories(int calories) {
		this.calories = calories;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}
