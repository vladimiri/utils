package ru.deta.healthy.jpa;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="settings")
public class Settings extends BaseId {
	/**
	 * 
	 */
	private static final long serialVersionUID = 76186131883592861L;
	private int dailyCalsLimit = 1500;

	public int getDailyCalsLimit() {
		return dailyCalsLimit;
	}

	public void setDailyCalsLimit(int dailyCalsLimit) {
		this.dailyCalsLimit = dailyCalsLimit;
	}
	
}
