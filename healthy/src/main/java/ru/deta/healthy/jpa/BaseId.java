package ru.deta.healthy.jpa;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@MappedSuperclass
public class BaseId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2545058417235585311L;
	
	@Id
	@GeneratedValue(generator="healthy_seq")
	@SequenceGenerator(name="healthy_seq",sequenceName="healthy_seq", allocationSize=1)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
