package ru.deta.healthy.jpa.util;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class JPAFilter implements Filter {
	private static Logger logger = null;
	private static ThreadLocal<EntityManager> localEntityManagers = new ThreadLocal<EntityManager>();
	private EntityManagerFactory emf;

	public void init(FilterConfig filterConfig) throws ServletException {
		logger = Logger.getLogger(JPAFilter.class);
		emf = Persistence.createEntityManagerFactory("healthy");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		EntityManager em = emf.createEntityManager();
		localEntityManagers.set(em);
		try {
			if(em != null)
				em.getTransaction().begin();
			chain.doFilter(request, response);
		} catch (Throwable e) {
			logger.error("Error while processing request", e);
			throw new RuntimeException(e);
		} finally {
			if(em.getTransaction() != null && em.getTransaction().isActive()) {
				if(((HttpServletResponse)response).getStatus() == 200)
					em.getTransaction().commit();
				else {
					try {
						em.getTransaction().rollback();
					} catch (Exception e) {
						logger.error("Error while rollback transaction", e);
					}
				}
			}
			try {
				if(em != null)
					em.clear();
			} catch (Exception e) {
				logger.error("Error while clearing EM",e);
			}
			if(em != null)
				em.close();
		}
	}

	public void destroy() {
		if(emf != null)
			emf.close();
	}

	public static EntityManager getEntityManager() {
		return localEntityManagers.get();
	}
}
