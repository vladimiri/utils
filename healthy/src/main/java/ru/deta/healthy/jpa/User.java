package ru.deta.healthy.jpa;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9195357043620981155L;
	@Id
	private String login;

	private String password;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="settings_id")
	private Settings settings = new Settings();
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Settings getSettings() {
		return settings;
	}
	public void setSettings(Settings settings) {
		this.settings = settings;
	}
	
	
	
}
