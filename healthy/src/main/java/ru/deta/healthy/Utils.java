package ru.deta.healthy;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Utils {
	private static final Logger logger = Logger.getLogger(Utils.class);
	
	public static String md5(String str) {
		MessageDigest m;
		try {
			m = MessageDigest.getInstance("MD5");
			m.update(str.getBytes("UTF-8"),0,str.length());
			return new BigInteger(1,m.digest()).toString(16);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Can't calculate md5 hash", e);
		}
		return null;
	}
}
