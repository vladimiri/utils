String.prototype.format = String.prototype.f = function () {
	var args = arguments;
	return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
		if (m == "{{") { return "{"; }
		if (m == "}}") { return "}"; }
		return args[n];
	});
};


var qs = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
	return query_string;
} ();

ko.bindingHandlers.date = {
		update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
			var value = valueAccessor();
			var allBindings = allBindingsAccessor();
			var valueUnwrapped = ko.utils.unwrapObservable(value);

			// Date formats: http://momentjs.com/docs/#/displaying/format/
			var pattern = allBindings.format || 'DD/MM/YYYY';

			var output = "-";
			if (valueUnwrapped !== null && valueUnwrapped !== undefined && (valueUnwrapped > 0 || valueUnwrapped.length > 0)) {
				output = moment(valueUnwrapped).format(pattern);
			}

			if ($(element).is("input") === true) {
				$(element).val(output);
			} else {
				$(element).text(output);
			}
		}
};

ko.bindingHandlers.datetimepicker = {
		init: function (element, valueAccessor, allBindingsAccessor) {
			var options = allBindingsAccessor().datetimepickerOptions;
			$(element).datetimepicker(options).on("dp.change", function (evntObj) {
				var moment = evntObj.date;
				var cval = valueAccessor();
				if(typeof cval != "function") {
					if(moment != null) {
						valueAccessor(moment.toDate());
					} else {
						valueAccessor(null);
					}
				} else {
					if(moment != null)
						cval(moment.toDate());
					else
						cval(null);
				}
			});
		},
		update: function (element, valueAccessor) {
			var value = ko.utils.unwrapObservable(valueAccessor());
//			$("#datetimepicker").data("DateTimePicker").date().toDate();
			if($(element).data("DateTimePicker"))
				$(element).data("DateTimePicker").date(new Date(value));
		}
};

ko.bindingHandlers.showModal = {
		init: function (element, valueAccessor) {
		},
		update: function (element, valueAccessor) {
			var value = valueAccessor();
			if (ko.utils.unwrapObservable(value)) {
				$(element).modal('show');
				// this is to focus input field inside dialog
				$("input", element).focus();
			}
			else {
				$(element).modal('hide');
			}
		}
};

ko.extenders.required = function(target, overrideMessage) { 
	//add some sub-observables to our observable
	target.isValid = ko.observable();
	target.validationMessage = ko.observable();

	//define a function to do validation
	function validate(newValue) {
		target.isValid(newValue ? true : false);
		target.validationMessage(newValue ? "" : overrideMessage || "This field is required");
	}

	//initial validation
	validate(target());
	//validate whenever the value changes
	target.subscribe(validate);

	//return the original observable
	return target;
};


ko.extenders.numeric = function(target, precision) {
    //create a writable computed observable to intercept writes to our observable
    var result = ko.pureComputed({
        read: target,  //always return the original observables value
        write: function(newValue) {
            var current = target(),
                roundingMultiplier = Math.pow(10, precision),
                newValueAsNum = isNaN(newValue) ? 0 : parseFloat(+newValue),
                valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;
 
            //only write if it changed
            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                //if the rounded value is the same, but a different value was written, force a notification for the current field
                if (newValue !== current) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    }).extend({ notify: 'always' });
 
    //initialize with current value to make sure it is rounded appropriately
    result(target());
 
    //return the new computed observable
    return result;
};



ko.bindingHandlers.templateInit = {
		init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
			if(viewModel && viewModel.templateInit) {
				viewModel.templateInit();
			}
		},
		update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
		}
};