

var viewModel = function (){
	var self = this;
	self.meal= ko.observable();
	self.mealDate = ko.observable();
	self.note = ko.observable();
	self.calories = ko.observable();
	self.statusMessage = ko.observable();

	var param = {type: "GET",url: "/rest/meal/{0}".f(qs.id),datatype:'json'};
	$.ajax(param)
	.done(function(status){
		if(status.Meal) {
			self.meal(status.Meal);
			self.mealDate(status.Meal.mealDate);
			self.note(status.Meal.note);
			self.calories(status.Meal.calories);
		}
	});

	self.doSave = function(){
		var data = ko.toJSON({MealParam: {mealDate: self.mealDate,note: self.note,calories: self.calories}});
		var param = {type: "PUT",url: "/rest/meal/{0}".f(qs.id),data: data,datatype:'json',contentType:"application/json; charset=utf-8"};
		$.ajax(param)
		.done(function(status){
			if(status.Meal) {
				self.statusMessage("Saved!");
			}
		})
		.fail(function(status){
			self.statusMessage(status.responseText);
		});
	};
	self.doDelete = function(){
		var param = {type: "DELETE",url: "/rest/meal/{0}".f(qs.id),datatype:'json'};
		$.ajax(param)
		.done(function(status){
			if(status.Meal) {
				statusMessage("Removed!");
			}
		})
		.fail(function(status){
			statusMessage(status.responseText);
		});
	};
};

ko.applyBindings(viewModel());
