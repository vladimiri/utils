var View = function(title, templateName, data, selected, render) {
	this.title = title;
	this.templateName = templateName;
	this.data = data; 
	this.isSelected = ko.computed(function() {
		return this === selected();  
	}, this);
	this.render = render;
};



var calendarModel = new function() {
	var self = this;

	this.dateFrom = ko.observable();
	this.dateTo = ko.observable();
	self.statusMessage= ko.observable();
	this.aggregate = ko.observable(false);
	
	this.dailyCalsLimit = ko.observable(1000);
	this.fromTime = ko.observable().extend({ required: "Please enter start time for aggregation" });
	this.toTime = ko.observable().extend({ required: "Please enter end time for aggregation" });
	
	this.aggregate.subscribe(function(newValue) {
		if(newValue) {
			self.dailyCalsLimit(registered().settings.dailyCalsLimit);
		} else {
			var calendar = self.calendarInit(self.getPlainData,"iframe");
			$('.btn-group button[data-calendar-nav]').each(function() {
				var $this = $(this);
				$this.click(function() {
					calendar.navigate($this.data('calendar-nav'));
				});
			});

			$('.btn-group button[data-calendar-view]').each(function() {
				var $this = $(this);
				$this.click(function() {
					calendar.view($this.data('calendar-view'));
				});
			});

			$('#first_day').change(function(){
				var value = $(this).val();
				value = value.length ? parseInt(value) : null;
				calendar.setOptions({first_day: value});
				calendar.view();
			});

			$('#language').change(function(){
				calendar.setLanguage($(this).val());
				calendar.view();
			});

			$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
				//e.preventDefault();
				//e.stopPropagation();
			});
		}
	});
	
	
	this.doAggregate = function() {
		var calendar = self.calendarInit(self.getAggregateData,"template");
		
		$('.btn-group button[data-calendar-nav]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.navigate($this.data('calendar-nav'));
			});
		});

		$('.btn-group button[data-calendar-view]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.view($this.data('calendar-view'));
			});
		});

		$('#first_day').change(function(){
			var value = $(this).val();
			value = value.length ? parseInt(value) : null;
			calendar.setOptions({first_day: value});
			calendar.view();
		});

		$('#language').change(function(){
			calendar.setLanguage($(this).val());
			calendar.view();
		});

		$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
			//e.preventDefault();
			//e.stopPropagation();
		});
	};
	
	
	
	self.getPlainData = function(from,to) {
		self.statusMessage("");
		from = moment(from).unix();
		to = moment(to).unix()
		$.ajax({type: "GET",url: "/rest/meal",data:{from:from,to:to},datatype:'json',async: false})
		.done(function(status){
			if(status.ArrayList) {
				 self.array = status.ArrayList.map(function (item) {
					item.title = item.note ? item.note : "Untitled";
					item.start = item.mealDate;
					item.end = item.mealDate + 1000*60*60;
					item.url = "meal.html?id="+item.id;
					item['class'] = 'event-info';
					return item;
				});
				/*
				 * .event-important {
background-color: #ad2121;
}
.event-info {
background-color: #1e90ff;
}
.event-warning {
background-color: #e3bc08;
}
.event-inverse {
background-color: #1b1b1b;
}
.event-success {
background-color: #006400;
}
.event-special {
background-color: #800080;
}
				 * {
		        	"id": "293",
		        	"title": "This is warning class event with very long title to check how it fits to evet in day view",
		        	"url": "http://www.example.com/",
		        	"class": "event-warning",
		        	"start": "1362938400000",
		        	"end":   "1363197686300"
		        }
				 */
			}
		})
		.fail(function(status){	
			self.statusMessage(status.responseText);
		});

		return self.array;
	};
	self.getAggregateData = function(from,to) {
		var array = self.getPlainData(from,to);
		var map = {};
		var out = [];
		
		var beginTime = parseInt(moment(self.fromTime()).format("HHmm"));
		var endTime = parseInt(moment(self.toTime()).format("HHmm"));
		
		for ( var i=0,j=array.length;i<j;i++ ) {
			var result = {};
			var mom = moment(array[i].mealDate);
			var day = parseInt(mom.format("YYYYMMDD"));
			var start = parseInt(mom.format("HHmm"));
			if(start > beginTime && start <= endTime) {
				if(map[day]) {
					map[day].calories += array[i].calories;
					map[day].note += "<br/>"+array[i].note;
				} else {
					map[day] = array[i];
				}
			}
		}
		var dailyCalsLimit = self.dailyCalsLimit();
		for (var day in map) {
			out.push(map[day]);
			if(map[day].calories > dailyCalsLimit) {
				map[day]["class"] = "event-warning";
				map[day].url = "";
			}
		}
		return out;
	};
	this.templateInit = function(){
		var calendar = this.calendarInit(this.getPlainData,"iframe");
		
		
		$('.btn-group button[data-calendar-nav]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.navigate($this.data('calendar-nav'));
			});
		});

		$('.btn-group button[data-calendar-view]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.view($this.data('calendar-view'));
			});
		});

		$('#first_day').change(function(){
			var value = $(this).val();
			value = value.length ? parseInt(value) : null;
			calendar.setOptions({first_day: value});
			calendar.view();
		});

		$('#language').change(function(){
			calendar.setLanguage($(this).val());
			calendar.view();
		});

		$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
			//e.preventDefault();
			//e.stopPropagation();
		});
	};

	this.calendarInit = function(dataHandler,modalType){
		var options = {
				events_source: dataHandler,
				view: 'month',
				tmpl_path: 'tmpls/',
				tmpl_cache: false,
//				day: moment(),
				modal: "#events-modal",
				modal_type: modalType,
				onAfterEventsLoad: function(events) {
					if(!events) {
						return;
					}
					var list = $('#eventlist');
					list.html('');

					$.each(events, function(key, val) {
						$(document.createElement('li'))
						.html('<a href="' + val.url + '">' + val.title + '</a>')
						.appendTo(list);
					});
				},
				onAfterViewLoad: function(view) {
					$('.page-header h3').text(this.getTitle());
					$('.btn-group button').removeClass('active');
					$('button[data-calendar-view="' + view + '"]').addClass('active');
				},
				classes: {
					months: {
						general: 'label'
					}
				}
		};

		var calendar = $('#calendar').calendar(options);
		return calendar;
	};
};


var loginModel = new function() {
	var self = this;
	self.login= ko.observable();
	self.password= ko.observable();
	self.user= ko.observable();
	self.statusMessage= ko.observable();
	self.doLogin = function(){
		self.statusMessage("");
		if(!self.login() || !self.password()) {
			this.statusMessage("Please fill all fields above");
			return;
		}
		var param = {type: "GET",url: "/rest/auth",data:{login:self.login(),password:self.password()},datatype:'json'};
		$.ajax(param)
		.done(function(status){
			if(status.User) {
				self.statusMessage("User {0} registered".f(status.User.login));
				rootModel.doLogIn(status.User);
			} else {
				self.statusMessage("User can't be created by unknown reason.");
			}
		})
		.fail(function(status){	
			self.statusMessage(status.responseText);
		});

	}
};


var addMealModel = new function() {
	var self= this;
	self.mealDate = ko.observable(new Date()).extend({ required: "Please enter date of meal" });
	self.note = ko.observable();
	self.calories = ko.observable(0).extend({ required: "Please enter number" });
	this.statusMessage= ko.observable();

	self.doAddMeal = function(){
		if(!self.mealDate.isValid() || !this.calories.isValid()) {
			this.statusMessage("Please correct errors above");
			return;
		}
		var data = ko.toJSON({MealParam: self});
		var param = ext({url: "/rest/meal",data:data});
		$.ajax(param)
		.done(function(status){
			if(status.Meal) {
				self.statusMessage("Meal {0} registered".f(status.Meal.id));
			} else {
				self.statusMessage("Meal can't be added because of {0}.".f(status.responseText));
			}
		})
		.fail(function(status){	
			self.statusMessage("Meal can't be added because of {0}.".f(status.responseText));
		});
	};
};


var registerModel = new function(login,password) {
	var uc = false;
	this.login= ko.observable(login).extend({ required: "Please enter first name" });
	this.password= ko.observable(password).extend({ required: "Please enter password" });
	this.userCreated=ko.observable(uc);
	this.statusMessage= ko.observable();

	this.doRegister= function(){
		this.statusMessage("");
		var self = this;
		if(!this.login.isValid() || !this.password.isValid()) {
			this.statusMessage("Please correct errors above");
			return;
		}
		var data = ko.toJSON({User:this});
		var param = ext({url: "/rest/auth",data:data});
		$.ajax(param)
		.done(function(status){
			if(status.User) {
				self.statusMessage("User with login {0} created".f(status.User.login));
				self.userCreated(true);
			} else {
				self.statusMessage("User can't be created by unknown reason.");
			}
		})
		.fail(function(status){	
			self.statusMessage(status.responseText);
		});
	};
};

var ext = function (extension) {
	var e = $.extend({},defaultPostParam);
	return $.extend(e,extension);
};

var defaultPostParam = {
		type:"POST",
		contentType:"application/json; charset=utf-8",
		dataType:"json",
};

var listMealModel = new function(){
	var self = this;
	self.myData = ko.observableArray();
	this.selectedItems = ko.observableArray();
	
	this.mealDate = ko.observable().extend({ required: "Please enter date of meal" });
	this.note = ko.observable();
	this.calories = ko.observable().extend({ required: "Please enter number of calories" });
	this.mealId = ko.observable();
	
	self.gridOptions = {
			data: self.myData, multiSelect: false, selectedItems: self.selectedItems, 
			columnDefs: [ 
				{ field: 'mealDate', displayName: 'Date of meal',cellFormatter : function(unixTimeTicks) { return new Date(unixTimeTicks); } },
				{ field: 'calories', displayName: 'Number of calories' },{ field: 'note', displayName: 'Note' }],
			showColumnMenu: false,
			displaySelectionCheckbox: false
		};
	this.statusMessage= ko.observable();


	//this.templateInit = function(){
	this.refresh = function(){
		$.ajax({
			url: '/rest/meal',
			type: 'GET',
			data: {},
			context: this,
			success: function (data) {
				self.myData(data.ArrayList);
			}
		});
	};
	this.refresh();
	
	this.doEdit = function(){
		if(self.selectedItems() && self.selectedItems().length > 0) {
			var item = self.selectedItems()[0];
			self.mealDate(item.mealDate);
			self.note(item.note);
			self.mealId(item.id);
			self.calories(item.calories);
		}
	}
	
	
	this.doRemove = function(){
		if(self.mealId() > 0) {
			$.ajax({
				url: '/rest/meal/{0}'.f(self.mealId()),
				type: 'DELETE',
				context: this,
				success: function (data) {
					self.selectedItems([]);
					self.mealId(0);
					self.refresh();
				},
				fail: function (status) {
					self.statusMessage(status.selectedText);
				}
			});
			
		}
	}
	
	this.doSave = function(){
		if(self.mealId() > 0) {
			var data = ko.toJSON({MealParam: {id: self.mealId(),calories: self.calories(),note: self.note(),mealDate: self.mealDate()}});
			$.ajax(ext({
				url: '/rest/meal/{0}'.f(self.mealId()),
				type: 'PUT',
				data: data,
				context: this,
				success: function (data) {
					self.selectedItems([]);
					self.mealId(0);
					self.refresh();
				},
				fail: function (status) {
					self.statusMessage(status.selectedText);
				}
			}));
			
		}
	}
}

var rootModel;
var viewModel = function () {
	var self = this;
	rootModel = self;
	self.selectedView= ko.observable();
	self.views= ko.observableArray([
	                                new View("Login", "loginTemplate", loginModel,self.selectedView),
	                                new View("Register", "registerTemplate", registerModel,self.selectedView)
	                                ]);
	self.registered = ko.observable();
	self.login = ko.observable();
	
	self.doLogIn = function(User) {
		self.views([
		            new View("Add meal", "addTemplate", addMealModel,rootModel.selectedView),
		            new View("List meals", "listTemplate", listMealModel,rootModel.selectedView),
		            new View("Meal calendar", "searchTemplate", calendarModel,rootModel.selectedView),
		            new View("User settings", "userSettingsTemplate", userSettingsModel,rootModel.selectedView)
		            ]);
		self.selectedView(self.views()[0]);
		self.registered(User);
		self.login(User.login);
	}
	
	
};


var userSettingsModel = new function() {
	var self = this;
	self.dailyCalsLimit = ko.observable().extend({ numeric: 0 });
	self.statusMessage = ko.observable();
	
	self.doSaveChanges = function() {
		var data = ko.toJSON({Settings : {dailyCalsLimit : self.dailyCalsLimit()}});
		var param = ext({type: "PUT",url: "/rest/user",data: data});
		$.ajax(param)
		.done(function(status){
			if(status.User) {
				registered(status.User);
				self.statusMessage("User {0} settings successfully saved.".f(status.User.login));
			}
		})
		.fail(function(status){
			self.statusMessage(status.responseText);
		});
	};
	
	self.templateInit = function(){
		self.dailyCalsLimit(rootModel.registered().settings.dailyCalsLimit);
	}
};



$('script[type="template/html"]').each(function(){
	$(this).hide();
	$(this).html($.ajax({
		type: "GET",
		url: $(this).attr("src"),
		cache: false,
		async: false,
		dataType: 'text'
	}).responseText);
});


ko.applyBindings(viewModel());

rootModel.selectedView(rootModel.views()[0]);
var param = {type: "GET",url: "/rest/auth/check",datatype:'json'};
$.ajax(param)
.done(function(status){
	if(status.User) {
		rootModel.doLogIn(status.User);
	}
});
