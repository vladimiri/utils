#!/bin/bash

./nginx -p ./ -s reload

sleep 10

kill -9 `ps -fU $USER| grep "nginx: worker process is shutting down" | grep -v grep | awk '{ print $2 }'`

