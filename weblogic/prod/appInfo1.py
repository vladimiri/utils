
from java.util import Calendar

def defineVariables():
  #Define all the variables used
  username='usssadmin'
  password='MAGus26abO'
  adminurl='t3://mn-slr01:9999'
  servername='mnWeb11'
  appname='ussweb'

def connectToAdminServer():
  #Connect to Admin Server
  connect(username,password,adminurl)

def extractAndPrintSessionInfo(servletSessionRuntime):
  #Get session name
  session_name=ssession.getName().split('!')[0]
  #Creation time is concatinated to the session name. So split it
  ct_millis=ssession.getName().split('!')[1]
  cal = Calendar.getInstance()
  cal.setTimeInMillis(Long(ct_millis))
  ct = cal.getTime()
  #Get last accessed time
  tla_millis=ssession.getTimeLastAccessed()
  cal.setTimeInMillis(Long(tla_millis))
  tla = cal.getTime()
  print ' '
  print 'Session Name - ' + session_name
  print 'Creation Time - ', ct
  print 'Last Accessed Time - ', tla

#Main
defineVariables()
connectToAdminServer()

#Get Runtime for our server
domainRuntime()
cd('/ServerRuntimes/'+servername)

#Get all running applications
apps=cmo.getApplicationRuntimes()
for app in apps:

  #We are intersted only on this application
  if app.getName() == appname:
  
    print 'Application Name - ',  app.getName()

    #Get all components in that application
    comps=app.getComponentRuntimes()
    for comp in comps:

      #We are interested in only web components
      if comp.getType() == 'WebAppComponentRuntime':
        comp_name = comp.getName().split(servername+'_/')
        if len(comp_name) == 1:
            display_comp_name = '(default web app)'
        else:
            display_comp_name = comp_name
        print 'Component Context Root - ', display_comp_name
  
        #Get all active sessions
        sessions=comp.getServletSessions()
        print 'Total no. of sessions - ', len(sessions)
 
        #Loop through all the available sessions
        for ssession in sessions:
            extractAndPrintSessionInfo(session)

#Disconnect and exit
disconnect()
exit()

