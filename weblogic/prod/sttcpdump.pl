#!/usr/bin/perl

$out = '/ALL_LOGS/bssoper';
$countRuns = 50000;
$packetCount = 3000000;

#open my $outfh, ">", "$out/output.txt" or die $!;
#my $oldfh = select $outfh;
$hn = `hostname`;
chomp $hn;
my $dt = qx#date +'%F_%H-%M-%S'#;
chomp $dt;
open (STDOUT,'>>',"$out/sttcpdump_${hn}_$dt.out");
open (STDERR,'>>',"$out/sttcpdump_${hn}_$dt.out");

print qx#date#;
print qx#lspci | grep Ether#;
print qx#ethtool -i eth0#;
print qx#ethtool -g eth0#;
print "cat /proc/sys/net/ipv4/conf/default/rp_filter\n";
print qx#cat /proc/sys/net/ipv4/conf/default/rp_filter#;
print "cat /proc/sys/net/ipv4/tcp_tso_win_divisor\n";
print qx#cat /proc/sys/net/ipv4/tcp_tso_win_divisor#;
print "route -n\n";
print qx#route -n#;
print "netstat -st\n";
print qx#netstat -st#;

for(1..$countRuns) {
	my $dt = qx#date +'%F_%H-%M-%S'#;
	chomp $dt;
	my $file = "tcpdump_${hn}_$dt.gz";
	print "Dumping to file $file\n";
	print qx#sudo tcpdump -c $packetCount -w - | gzip > '$out/$file'#;
	print "netstat -st\n";
	print qx#netstat -st#;
}

#select $oldfh;
#close $outfh;
