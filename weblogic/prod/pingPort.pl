
use IO::Socket;
use Time::HiRes qw/ time sleep /;

while(1) {
	my $start = time;
	my $sock = new IO::Socket::INET (
                                  PeerAddr => 'mn-slr02.vimpelcom.ru',
                                  PeerPort => '5556',
                                  Proto => 'tcp',
                                 );
	print "Could not create socket: $!\n" unless $sock;
	close($sock) if $sock;
	my $end = time;
	my $res = $end - $start;
	print <<EOF;
Connection created in $res msecs.
EOF
}