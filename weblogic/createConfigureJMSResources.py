# WLST Script to create and configure JMS resources.
#
# Author: Adrianos Dadis (http://sourcevirtues.wordpress.com/)
#
# Please for any improvements or corrections add a comment at http://sourcevirtues.wordpress.com/
#
# You are free do modify and distribute this script as you like.
#
# This script is for Weblogic WLST, which is a proprietary product of Oracle.
# Please use JBoss AS if it is possible, as it is as good as Weblogic and the source code is available.

from java.io import FileInputStream
from java.util import Properties

# Load the properties file.
def loadProperties(fileName):
	properties = Properties()
	input = FileInputStream(fileName)
	properties.load(input)
	input.close()

	result= {}
	for entry in properties.entrySet(): result[entry.key] = entry.value

	return result


properties = loadProperties("local.properties")

username = properties['username']
password = properties['password']
url = properties['providerURL']

# Connect to Admin Server
connect(username, password, url)
adminServerName = cmo.adminServerName

try:
	print "Give a try ..."
	domainName = cmo.name
	cd("Servers/%s" % adminServerName)
	adminServer = cmo

	# Delete old resources, if they exist.
	def deleteIgnoringExceptions(mbean):
		try: delete(mbean)
		except: pass
	
	def startTransaction():
		edit()
		startEdit()

	def endTransaction():
		save()
		activate(block="true")

	
	def createDLQ(qname, qjndiname):
		print "createDLQ() START"
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName)
		errorQueue = create(qname, "Queue")
		errorQueue.JNDIName = qjndiname

		# Set JMS Subdeployment		
		errorQueue.subDeploymentName = subDeploymentName
		
		# Set redeliver limit and do LOG when max redeliver times occured
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryFailureParams/'+qname)
		cmo.setRedeliveryLimit(1)
		cmo.setExpirationPolicy('Log')
		
		# Set delay (miliseconds) on redeliver
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryParamsOverrides/'+qname)
		cmo.setRedeliveryDelay(5000)

		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryFailureParams/'+qname)
		cmo.unSet('ErrorDestination')

		# Set Persistenr Delivery Mode
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryParamsOverrides/'+qname)
		cmo.setPriority(-1)
		cmo.setDeliveryMode('Persistent')
		cmo.setTimeToDeliver('-1')
		cmo.setTimeToLive(-1)
		print "createDLQ() END"
	
	
	def createQueue(qname, qjndiname, dlqName):
		print "createQueue() START"
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName)
		q1 = create(qname, "Queue")
		q1.JNDIName = qjndiname
		
		# Set JMS Subdeployment		
		q1.subDeploymentName = subDeploymentName

		# Set delay (miliseconds) on redeliver
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryParamsOverrides/'+qname)
		cmo.setRedeliveryDelay(2000)

		print "Assign failed destination to queue"
		# Set redeliver limit
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryFailureParams/'+qname)
		cmo.setRedeliveryLimit(3)
		# Set Failed Destination Delivery to another queue
		cmo.setExpirationPolicy('Redirect')
		erQueue = getMBean('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+dlqName)
		cmo.setErrorDestination(erQueue)

		# Enable Logging for Queue
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/MessageLoggingParams/'+qname)
		cmo.setMessageLoggingEnabled(true)
		cmo.setMessageLoggingFormat('JMSDestination,JMSMessageID')

		# Set Persistenr Delivery Mode
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName+'/Queues/'+qname+'/DeliveryParamsOverrides/'+qname)
		cmo.setPriority(-1)
		cmo.setDeliveryMode('Persistent')
		cmo.setTimeToDeliver('-1')
		cmo.setTimeToLive(-1)
		print "createQueue() END"


	def createCF(cfname, cfjndiname, xaEnabled):
		print "createCF() START"
		cd('/JMSSystemResources/'+moduleName+'/JMSResource/'+moduleName)
		cf = create(cfname, "ConnectionFactory")
		cf.JNDIName = cfjndiname
		cf.subDeploymentName = subDeploymentName
		# Set XA transactions enabled
		if (xaEnabled == "true"):
			cf.transactionParams.setXAConnectionFactoryEnabled(1)
		print "createCF() END"


	def createJMSModule():
		print "createJMSModule() START"
		cd('/JMSSystemResources')
		jmsModule = create(moduleName, "JMSSystemResource")
		jmsModule.targets = (adminServer,)	
	
		cd(jmsModule.name)
		# Create and configure JMS Subdeployment for this JMS System Module		
		sd = create(subDeploymentName, "SubDeployment")	
		myJMSServer = getMBean('/JMSServers/' + jmsServerName)
		sd.targets = (myJMSServer,)
		print "createJMSModule() END"
		
	
	def createJMSServer():
		print "createJMSServer() START"
		startTransaction()
		# Delete existing JMS Server and its Filestore
		cd("/JMSServers")
		deleteIgnoringExceptions(jmsServerName)
		cd("/FileStores")
		deleteIgnoringExceptions(fileStoreName)

		# Create Filestore
		cd('/')
		filestore = cmo.createFileStore(fileStoreName)
		filestore.setDirectory(properties['fileStoreDirecory'])
		filestore.targets = (adminServer,)
		endTransaction()
		
		startTransaction()
		cd("/JMSServers")
		# Create JMS server and assign the Filestore
		jmsServer = create(jmsServerName, "JMSServer")
		jmsServer.setPersistentStore(filestore)
		jmsServer.targets = (adminServer,)
		endTransaction()
		print "createJMSServer() END"
	
	
	
	moduleName = 'MyJMSSModule'
	subDeploymentName = 'MyJMSSubDeployment'
	fileStoreName = 'MyFileStore1'
	jmsServerName = 'MyJMSServer1'


	startTransaction()
	# Delete existing JMS Module
	cd("/JMSSystemResources")
	deleteIgnoringExceptions(moduleName)
	endTransaction()

	print "Create JMS Server"
	createJMSServer()
	
	startTransaction()
	print "Create JMS Module"
	createJMSModule()
	
	# Create an Error Destination Queue to redirect messages to this queue,
	# when the message(s) has reached the maximum redelivered times.
	queueEDQName1 = 'myRequestQueueEDQ'
	queueEDQJNDIName1 = 'jms/my/requestQueueEDQ'
	print "Create Error Destinantion Queue"
	createDLQ(queueEDQName1, queueEDQJNDIName1)
	
	queueName1 = 'myRequestQueue'
	queueJNDIName1 = 'jms/my/requestQueue'
	print "Create Queue"
	createQueue(queueName1, queueJNDIName1, queueEDQName1)
	
	myCFName = 'my-jmsXACF'
	myCFJNDIName = 'jms/my/jmsXACF'
	print "Create JMS XA Connection Factory"
	createCF(myCFName, myCFJNDIName, "true")
	
	
	endTransaction()
except:
	dumpStack()
	cancelEdit("y")
	#raise

print "disconnect"
disconnect()

print "exit"
exit()

