"""
This is a WLST script that is generated by the WebLogic Scripting Tool (WLST)
Configuration file converted               : C:/dev/domains/domain2/config/config.xml
WLST script generated to file              : C:/dev/domains/domain2/config/config.py
Properties file associated with the script : C:/dev/domains/domain2/config/config.py.properties

This script will first try to connect to a running server using the 
values in the properties file. If there is no server running, WLST
will start a server with the values in the properties file. You should change
these values to suit your environmental needs. After running the script, 
the server that is started(if started one) will be shutdown. 
This might exit you from your WLST shell."""

from weblogic.descriptor import BeanAlreadyExistsException
from java.lang.reflect import UndeclaredThrowableException
from java.lang import System
import javax
from javax import management
from javax.management import MBeanException
from javax.management import RuntimeMBeanException
import javax.management.MBeanException
from java.lang import UnsupportedOperationException


def initConfigToScriptRun():
  global startedNewServer
  loadProperties("C:/dev/domains/domain2/./config/config.py.properties")
  hideDisplay()
  hideDumpStack("true")
  # try connecting to a running server if it is already running ... 
  if connected=="false":
    try:
      URL="t3://"+adminServerListenAddress+":"+adminServerListenPort
      connect(userName, passWord, URL, userConfigFile="C:/dev/domains/domain2/./config/c2sConfigmydomain", userKeyFile="C:/dev/domains/domain2/./config/c2sSecretmydomain")
    except WLSTException:
      print 'No server is running at '+URL+', the script will start a new server'
  hideDumpStack("false")
  if connected=="false":
    print 'Starting a brand new server at '+URL+' with server name '+adminServerName
    print 'Please see the server log files for startup messages available at '+domainDir
    # If a config.xml exists in the domainDir, WLST will use that config.xml to bring up the server. 
    # If you would like WLST to overwrite this directory, you should specify overWriteRootDir='true' as shown below
    # startServer(adminServerName, domName, URL, userName, passWord,domainDir, overWriteRootDir='true')
    _timeOut = Integer(TimeOut)
    # If you want to specify additional JVM arguments, set them using startServerJvmArgs in the property file or below
    _startServerJvmArgs=startServerJvmArgs
    if (_startServerJvmArgs=="" and (System.getProperty("java.vendor").find("Sun")>=0 or System.getProperty("java.vendor").find("Oracle")>=0 or System.getProperty("java.vendor").find("Hewlett")>=0)):
      _startServerJvmArgs = " -XX:MaxPermSize=256m"
    if overWriteRootDir=='true':
      startServer(adminServerName, domName, URL, userName, passWord,domainDir, timeout=_timeOut.intValue(), overWriteRootDir='true', block='true', jvmArgs=_startServerJvmArgs)
    else:
      startServer(adminServerName, domName, URL, userName, passWord,domainDir, timeout=_timeOut.intValue(), block='true', jvmArgs=_startServerJvmArgs)
    startedNewServer=1
    print "Started Server. Trying to connect to the server ... "
    connect(userName, passWord, URL, userConfigFile="C:/dev/domains/domain2/./config/c2sConfigmydomain", userKeyFile="C:/dev/domains/domain2/./config/c2sSecretmydomain")
    if connected=='false':
      stopExecution('You need to be connected.')

def startTransaction():
  edit()
  startEdit()

def endTransaction():
  startEdit()
  save()
  activate(block="true")

from javax.management import InstanceAlreadyExistsException
from java.lang import Exception
from jarray import array

def endOfConfigToScriptRun():
  global startedNewServer
  #Save the changes you have made
  # shutdown the server you have started
  if startedNewServer==1:
    print 'Shutting down the server that is started... '
    shutdown(force='true', block='true')
  print 'Done executing the script.'

def create_Server_0(path, beanName):
  cd(path)
  try:
    print "creating mbean of type Server ... "
    theBean = cmo.lookupServer(beanName)
    if theBean == None:
      cmo.createServer(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_Realm_2(path, beanName):
  cd(path)
  try:
    print "creating mbean of type Realm ... "
    theBean = cmo.lookupRealm(beanName)
    if theBean == None:
      cmo.createRealm(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_Authorizer_4(path, beanName):
  cd(path)
  try:
    print "creating mbean of type Authorizer ... "
    theBean = cmo.lookupAuthorizer(beanName)
    if theBean == None:
      cmo.createAuthorizer(beanName,"weblogic.security.providers.xacml.authorization.XACMLAuthorizer")
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_Adjudicator_6(path, beanName):
  cd(path)
  try:
    print "creating mbean of type Adjudicator ... "
    theBean = cmo.getAdjudicator()
    if theBean == None:
      cmo.createAdjudicator(beanName,"weblogic.security.providers.authorization.DefaultAdjudicator")
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_AuthenticationProvider_8(path, beanName):
  cd(path)
  try:
    print "creating mbean of type AuthenticationProvider ... "
    theBean = cmo.lookupAuthenticationProvider(beanName)
    if theBean == None:
      cmo.createAuthenticationProvider(beanName,"weblogic.security.providers.authentication.DefaultAuthenticator")
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_CertPathProvider_12(path, beanName):
  cd(path)
  try:
    print "creating mbean of type CertPathProvider ... "
    theBean = cmo.lookupCertPathProvider(beanName)
    if theBean == None:
      cmo.createCertPathProvider(beanName,"weblogic.security.providers.pk.WebLogicCertPathProvider")
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_CredentialMapper_14(path, beanName):
  cd(path)
  try:
    print "creating mbean of type CredentialMapper ... "
    theBean = cmo.lookupCredentialMapper(beanName)
    if theBean == None:
      cmo.createCredentialMapper(beanName,"weblogic.security.providers.credentials.DefaultCredentialMapper")
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_RoleMapper_16(path, beanName):
  cd(path)
  try:
    print "creating mbean of type RoleMapper ... "
    theBean = cmo.lookupRoleMapper(beanName)
    if theBean == None:
      cmo.createRoleMapper(beanName,"weblogic.security.providers.xacml.authorization.XACMLRoleMapper")
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_ForeignJNDIProvider_18(path, beanName):
  cd(path)
  try:
    print "creating mbean of type ForeignJNDIProvider ... "
    theBean = cmo.lookupForeignJNDIProvider(beanName)
    if theBean == None:
      cmo.createForeignJNDIProvider(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_ForeignJNDILink_20(path, beanName):
  cd(path)
  try:
    print "creating mbean of type ForeignJNDILink ... "
    theBean = cmo.lookupForeignJNDILink(beanName)
    if theBean == None:
      cmo.createForeignJNDILink(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_JDBCSystemResource_34(path, beanName):
  cd(path)
  try:
    print "creating mbean of type JDBCSystemResource ... "
    theBean = cmo.lookupJDBCSystemResource(beanName)
    if theBean == None:
      cmo.createJDBCSystemResource(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_Property_36(path, beanName):
  cd(path)
  try:
    print "creating mbean of type Property ... "
    theBean = cmo.lookupProperty(beanName)
    if theBean == None:
      cmo.createProperty(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass
  except TypeError:
    prop = cmo.createProperty()
    prop.setName(beanName)

def create_JMSSystemResource_38(path, beanName):
  cd(path)
  try:
    print "creating mbean of type JMSSystemResource ... "
    theBean = cmo.lookupJMSSystemResource(beanName)
    if theBean == None:
      cmo.createJMSSystemResource(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_ConnectionFactory_40(path, beanName):
  cd(path)
  try:
    print "creating mbean of type ConnectionFactory ... "
    theBean = cmo.lookupConnectionFactory(beanName)
    if theBean == None:
      cmo.createConnectionFactory(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def create_UniformDistributedQueue_42(path, beanName):
  cd(path)
  try:
    print "creating mbean of type UniformDistributedQueue ... "
    theBean = cmo.lookupUniformDistributedQueue(beanName)
    if theBean == None:
      cmo.createUniformDistributedQueue(beanName)
  except java.lang.UnsupportedOperationException, usoe:
    pass
  except weblogic.descriptor.BeanAlreadyExistsException,bae:
    pass
  except java.lang.reflect.UndeclaredThrowableException,udt:
    pass

def setAttributesFor_jms_RPJMSConnectionFactory_41():
  cd("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule/ConnectionFactories/jms/RPJMSConnectionFactory")
  print "setting attributes for mbean type JMSConnectionFactory"
  set("DefaultTargetingEnabled", "true")
  set("Name", "jms/RPJMSConnectionFactory")
  set("JNDIName", "jms/RPJMSConnectionFactory")

def setAttributes_SecurityConfiguration_44():
  cd("/SecurityConfiguration/mydomain")
  print "setting attributes for mbean type SecurityConfiguration"
  setEncrypted("Credential", "c2s45", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  setEncrypted("NodeManagerPassword", "c2s46", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  setEncrypted("NodeManagerPassword", "c2s47", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  set("NodeManagerUsername", "admin123")
  setEncrypted("Credential", "c2s48", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")

def setAttributes_JDBCConnectionPoolParams_49():
  cd("/JDBCSystemResources/jdbc/rpPoolData/JDBCResource/jdbc/rpPoolData/JDBCConnectionPoolParams/jdbc/rpPoolData")
  print "setting attributes for mbean type JDBCConnectionPoolParams"
  set("TestTableName", "SQL ISVALID")

def setAttributes_SecurityParams_50():
  cd("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule/ConnectionFactories/jms/RPJMSConnectionFactory/SecurityParams/jms/RPJMSConnectionFactory")
  print "setting attributes for mbean type SecurityParams"
  set("AttachJMSXUserId", "false")

def setAttributes_JDBCDriverParams_51():
  cd("/JDBCSystemResources/jdbc/rpPoolData/JDBCResource/jdbc/rpPoolData/JDBCDriverParams/jdbc/rpPoolData")
  print "setting attributes for mbean type JDBCDriverParams"
  setEncrypted("Password", "c2s52", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  setEncrypted("Password", "c2s53", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  set("DriverName", "oracle.jdbc.xa.client.OracleXADataSource")
  set("Url", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=mn-bssscan)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=BSSDEV)))")

def setAttributes_Domain_54():
  cd("/")
  print "setting attributes for mbean type Domain"
  set("DomainVersion", "12.1.3.0.0")
  set("ConfigurationVersion", "12.1.3.0.0")
  set("AdminServerName", "myserver")

def setAttributesFor_GeneralService_23():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/GeneralService")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#ru/atc/uss/general/GeneralService")
  set("LocalJNDIName", "BSSU#ru/atc/uss/general/GeneralService")

def setAttributesFor_jdbc_rpPoolData_35():
  cd("/JDBCSystemResources/jdbc/rpPoolData")
  print "setting attributes for mbean type JDBCSystemResource"
  refBean0 = getMBean("/Servers/myserver")
  theValue = jarray.array([refBean0], Class.forName("weblogic.management.configuration.TargetMBean"))
  cmo.setTargets(theValue)


def setAttributes_TransactionParams_55():
  cd("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule/ConnectionFactories/jms/RPJMSConnectionFactory/TransactionParams/jms/RPJMSConnectionFactory")
  print "setting attributes for mbean type TransactionParams"
  set("XAConnectionFactoryEnabled", "true")

def setAttributesFor_USSAPP_JNDI_Provider_19():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider")
  print "setting attributes for mbean type ForeignJNDIProvider"
  set("User", "admin123")
  setEncrypted("Password", "c2s56", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  set("ProviderURL", "t3://dev-app01.vimpelcom.ru:46001/")
  setEncrypted("Password", "c2s57", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  refBean0 = getMBean("/Servers/myserver")
  theValue = jarray.array([refBean0], Class.forName("weblogic.management.configuration.TargetMBean"))
  cmo.setTargets(theValue)


def setAttributesFor_VIPSubscriberServices_29():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/VIPSubscriberServices")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#com/amdocs/css/vip/subscriber/ejb/VIPSubscriberServices")
  set("LocalJNDIName", "BSSU#com/amdocs/css/vip/subscriber/ejb/VIPSubscriberServices")

def setAttributesFor_myserver_1():
  cd("/Servers/myserver")
  print "setting attributes for mbean type Server"
  set("ListenAddress", "")

def setAttributesFor_jms_RPQueue_43():
  cd("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule/UniformDistributedQueues/jms/RPQueue")
  print "setting attributes for mbean type UniformDistributedQueue"
  set("DefaultTargetingEnabled", "true")
  set("Name", "jms/RPQueue")
  set("JNDIName", "jms/RPQueue")

def setAttributes_JDBCDataSource_58():
  cd("/JDBCSystemResources/jdbc/rpPoolData/JDBCResource/jdbc/rpPoolData")
  print "setting attributes for mbean type JDBCDataSource"
  set("Name", "jdbc/rpPoolData")

def setAttributes_JDBCDataSourceParams_59():
  cd("/JDBCSystemResources/jdbc/rpPoolData/JDBCResource/jdbc/rpPoolData/JDBCDataSourceParams/jdbc/rpPoolData")
  print "setting attributes for mbean type JDBCDataSourceParams"
  set("GlobalTransactionsProtocol", "TwoPhaseCommit")
  set("JNDINames", jarray.array(["jdbc/rpPoolData"], String))

def setAttributes_ClientParams_60():
  cd("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule/ConnectionFactories/jms/RPJMSConnectionFactory/ClientParams/jms/RPJMSConnectionFactory")
  print "setting attributes for mbean type ClientParams"
  set("MessagesMaximum", "10")
  set("SubscriptionSharingPolicy", "Exclusive")
  set("ClientIdPolicy", "Restricted")

def setAttributesFor_DefaultIdentityAsserter_11():
  cd("/SecurityConfiguration/mydomain/Realms/myrealm/AuthenticationProviders/DefaultIdentityAsserter")
  print "setting attributes for mbean type DefaultIdentityAsserter"
  set("ActiveTypes", jarray.array(["AuthenticatedUser"], String))

def setAttributesFor_RPJmsModule_39():
  cd("/JMSSystemResources/RPJmsModule")
  print "setting attributes for mbean type JMSSystemResource"
  refBean0 = getMBean("/Servers/myserver")
  theValue = jarray.array([refBean0], Class.forName("weblogic.management.configuration.TargetMBean"))
  cmo.setTargets(theValue)


def setAttributesFor_user_37():
  cd("/JDBCSystemResources/jdbc/rpPoolData/JDBCResource/jdbc/rpPoolData/JDBCDriverParams/jdbc/rpPoolData/Properties/jdbc/rpPoolData/Properties/user")
  print "setting attributes for mbean type JDBCProperty"
  set("Value", "BSSRP4")
  set("Name", "user")

def setAttributesFor_VIPAccountServices_27():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/VIPAccountServices")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#com/amdocs/css/vip/account/ejb/VIPAccountServices")
  set("LocalJNDIName", "BSSU#com/amdocs/css/vip/account/ejb/VIPAccountServices")

def setAttributes_EmbeddedLDAP_61():
  cd("/EmbeddedLDAP/mydomain")
  print "setting attributes for mbean type EmbeddedLDAP"
  setEncrypted("Credential", "c2s62", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")
  setEncrypted("Credential", "c2s63", "C:/dev/domains/domain2/./config/c2sConfigmydomain", "C:/dev/domains/domain2/./config/c2sSecretmydomain")

def setAttributesFor_NewOfferingService_33():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/NewOfferingService")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#com/amdocs/css/vip/offering/newoffering/ejb/NewOfferingService")
  set("LocalJNDIName", "BSSU#com/amdocs/css/vip/offering/newoffering/ejb/NewOfferingService")

def setAttributesFor_VIPCustomServices_21():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/VIPCustomServices")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#com/amdocs/css/vip/account/data/VIPCustomServices")
  set("LocalJNDIName", "BSSU#com/amdocs/css/vip/account/data/VIPCustomServices")

def setAttributesFor_VIPConfiguration_25():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/VIPConfiguration")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#com/amdocs/css/vip/config/VIPConfiguration")
  set("LocalJNDIName", "BSSU#com/amdocs/css/vip/config/VIPConfiguration")

def setAttributesFor_VIPQueueSender_31():
  cd("/ForeignJNDIProviders/USSAPP JNDI Provider/ForeignJNDILinks/VIPQueueSender")
  print "setting attributes for mbean type ForeignJNDILink"
  set("RemoteJNDIName", "BSSU#com/amdocs/css/vip/eai/ejb/VIPQueueSender")
  set("LocalJNDIName", "BSSU#com/amdocs/css/vip/eai/ejb/VIPQueueSender")

def deploy_rp_64():
  try:
    deploy("rp","Y:/rp/target/rp","myserver",stageMode="nostage",securityModel="DDOnly",block="true")
  except:
    print "Could not deploy application rp"

try:
  initConfigToScriptRun()
  startTransaction()
  create_Server_0("/", "myserver")
  create_Realm_2("/SecurityConfiguration/mydomain", "myrealm")
  create_Authorizer_4("/SecurityConfiguration/mydomain/Realms/myrealm", "XACMLAuthorizer")
  create_Adjudicator_6("/SecurityConfiguration/mydomain/Realms/myrealm", "DefaultAdjudicator")
  create_AuthenticationProvider_8("/SecurityConfiguration/mydomain/Realms/myrealm", "DefaultAuthenticator")
  create_AuthenticationProvider_8("/SecurityConfiguration/mydomain/Realms/myrealm", "DefaultIdentityAsserter")
  create_CertPathProvider_12("/SecurityConfiguration/mydomain/Realms/myrealm", "WebLogicCertPathProvider")
  create_CredentialMapper_14("/SecurityConfiguration/mydomain/Realms/myrealm", "DefaultCredentialMapper")
  create_RoleMapper_16("/SecurityConfiguration/mydomain/Realms/myrealm", "XACMLRoleMapper")
  create_ForeignJNDIProvider_18("/", "USSAPP JNDI Provider")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "VIPCustomServices")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "GeneralService")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "VIPConfiguration")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "VIPAccountServices")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "VIPSubscriberServices")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "VIPQueueSender")
  create_ForeignJNDILink_20("/ForeignJNDIProviders/USSAPP JNDI Provider", "NewOfferingService")
  create_JDBCSystemResource_34("/", "jdbc/rpPoolData")
  create_Property_36("/JDBCSystemResources/jdbc/rpPoolData/JDBCResource/jdbc/rpPoolData/JDBCDriverParams/jdbc/rpPoolData/Properties/jdbc/rpPoolData", "user")
  create_JMSSystemResource_38("/", "RPJmsModule")
  create_ConnectionFactory_40("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule", "jms/RPJMSConnectionFactory")
  create_UniformDistributedQueue_42("/JMSSystemResources/RPJmsModule/JMSResource/RPJmsModule", "jms/RPQueue")
  setAttributesFor_myserver_1()
  setAttributesFor_DefaultIdentityAsserter_11()
  setAttributesFor_USSAPP_JNDI_Provider_19()
  setAttributesFor_VIPCustomServices_21()
  setAttributesFor_GeneralService_23()
  setAttributesFor_VIPConfiguration_25()
  setAttributesFor_VIPAccountServices_27()
  setAttributesFor_VIPSubscriberServices_29()
  setAttributesFor_VIPQueueSender_31()
  setAttributesFor_NewOfferingService_33()
  setAttributesFor_jdbc_rpPoolData_35()
  setAttributesFor_user_37()
  setAttributesFor_RPJmsModule_39()
  setAttributesFor_jms_RPJMSConnectionFactory_41()
  setAttributesFor_jms_RPQueue_43()
  setAttributes_SecurityConfiguration_44()
  setAttributes_JDBCConnectionPoolParams_49()
  setAttributes_SecurityParams_50()
  setAttributes_JDBCDriverParams_51()
  setAttributes_Domain_54()
  setAttributes_TransactionParams_55()
  setAttributes_JDBCDataSource_58()
  setAttributes_JDBCDataSourceParams_59()
  setAttributes_ClientParams_60()
  setAttributes_EmbeddedLDAP_61()
  endTransaction()
  deploy_rp_64()
finally:
  endOfConfigToScriptRun()
